<?php
namespace Controller;
use Core\Controller as BaseController;

class Gallery extends BaseController
{
    public function __construct($route, $countRoute)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'gallery') {
                $this->index();
            }
        }
    }

    public function index(){
        $this->renderView("Pages/gallery", "gallery", $this->result);
    }
}