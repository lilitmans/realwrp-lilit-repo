<?php
namespace Controller;
use Core\Controller as BaseController;



class Announcement extends BaseController{
    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'announcement') {
                $this->index();
            }
        }
    }

    public function index()
    {
        $this->renderView("Pages/announcement","announcement", $this->result);
    }
}