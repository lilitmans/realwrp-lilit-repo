<?php
namespace Controller;
use Core\Controller as BaseController;

class Contacts extends BaseController
{
    public function __construct($route, $countRoute)
    {
        parent::__construct();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'contacts') {
                $this->index();
            }
        }
    }

    public function index(){
        $this->renderView("Pages/contacts", "contacts", $this->result);
    }
}