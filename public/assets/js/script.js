$(document).ready(function(){
    $('.slider-wrapper').slick({
        'setting-name': 'setting-value',
        slidesToShow: 2.5,
        slidesToScroll: 1,
        infinite: true,
        speed: 500,
        outline: 'none',
    });

    let questionBox = $('#_faq-box').children('figure').children('.question-box');

    questionBox.on('click', function() {
        let plusIcon = $(this).children('.icon-box').children('i');

        $(this).siblings(".response-box").slideToggle({ "opacity" : "show"}, 500);
        plusIcon.toggleClass('rotate-x');
        console.log($(this));
    });

    $('#_donation-amount div').on('click', function() {
        $(this).siblings().removeClass('selected-donation-amount');
        $(this).toggleClass('selected-donation-amount');
    });

    let messageTextarea = $('#_message');
    messageTextarea.val(null);

    $('#_recurrence-select').on('click', function () {
        $(this).children('i').toggleClass('rotate-v');
        $(this).siblings('#_select-options-box').slideToggle({ "opacity" : "show"}, 500);
    });

    $('#_select-options-box').children('div').on('click', function () {
        let selectedVal = $(this).text();
        let thisParent = $(this).parent('#_select-options-box');
        thisParent.siblings('#_recurrence-select').children('span').text(selectedVal);
        thisParent.slideToggle();
    });

    $('.menu-icon-box').on('click', function () {
        $(this).siblings('.header-content').slideToggle({ "opacity" : "show"}, 500);
    });
});
