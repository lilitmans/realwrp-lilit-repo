<section class="bg-opacity">
    <div class="content clear">
        <div class="content-box">
            <h1>Պատկերասրահ</h1>
        </div>
    </div>
</section>
<section id="_gallery-items-main-list">
    <div class="content clear">
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բոլորը</a></button>
            <button class="btn-bordered"><a href="#">Նկարներ</a></button>
            <button class="btn-bordered"><a href="#">Տեսանյութ</a></button>
        </div>
        <div class="flex-content">
            <div>
                <a href="<?=$baseurl ?>/gallery-item">
                <iframe height="530" src="https://www.youtube.com/embed/4poxgP96FSs"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <p>«Ընդդեմ գենդերային բռնության ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր
                    անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր...</p>
                </a>
            </div>
            <div>
                <a href="<?=$baseurl ?>/gallery-item">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p>«Ընդդեմ գենդերային բռնության ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր
                        անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր...</p>
                </a>
            </div>
            <div>
                <a href="<?=$baseurl ?>/gallery-item">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p>«Ընդդեմ գենդերային բռնության ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր
                        անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր...</p>
                </a>
            </div>
            <div>
                <a href="<?=$baseurl ?>/gallery-item">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p>«Ընդդեմ գենդերային բռնության ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր
                        անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր...</p>
                </a>
            </div>
            <div>
                <a href="<?=$baseurl ?>/gallery-item">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p>«Ընդդեմ գենդերային բռնության ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր
                        անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր...</p>
                </a>
            </div>

        </div>
    </div>
</section>