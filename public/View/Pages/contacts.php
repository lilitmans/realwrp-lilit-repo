<section class="bg-opacity">
    <div class="content clear">
        <div class="content-box">
            <h1>Contacts</h1>
        </div>
    </div>
</section>
<section>
    <div class="content clear">
        <form action="POST" method="">
            <div id="_contacts-box">
                <div>
                    <label for="_name">Your Name *</label>
                    <input type="text" id="_name" class="bordered-field" placeholder="Name">
                </div>
                <div>
                    <label for="_email">Email address</label>
                    <input type="email" id="_email" class="bordered-field" placeholder="Email">
                </div>
                <div>
                    <label for="_phone">Phone</label>
                    <input type="text" id="_phone" class="bordered-field" placeholder="Phone">
                </div>
                <div>
                    <label for="_message">Message</label>
                    <textarea name="" id="_message" class="bordered-field" cols="30" rows="10" placeholder="Your custom message text...">

                    </textarea>
                </div>
                <div class="buttons-box submit-btn-box">
                    <button class="btn-colored"><a href="#">Send</a></button>
                </div>
            </div>
        </form>
    </div>
</section>