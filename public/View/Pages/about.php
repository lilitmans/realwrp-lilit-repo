<section id="_about-us-main-page" class="bg-opacity">
    <div class="content main-heading-box clear">
        <div class="img-box">
            <img src="<?=$baseurl?>/assets/img/trees.png" alt="Background Image">
        </div>
        <div class="content-box">
            <h1>«Իրական Աշխարհ, Իրական Մարդիկ» հասարակական կազմակերպություն</h1>
            <p class="main-description">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ, քանի
                դեռ մեր հասարակությանը բնորոշ կլինեն անտարբերությունն ու մերժողականությունը.</p>
        </div>
        <div class="buttons-box">
            <button class="btn-important"><a href="#">DONATE</a></button>
            <button class="btn-colored"><a href="#">Կապվել Մեզ հետ</a></button>
        </div>
    </div>
</section>

<section id="_our-team-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Տեսլական, առաքելություն և արժեքներ</h4>
            <p class="txt-content-wide">ՄԻԱՎ-ով ապրող մարդկանց և նրանց ընտանիքների կյանքի որակի բարելավում` ինքնօգնության և փոխօգնության
                    շարժման հզորացում, ՄԻԱՎ դրական հանրույթի համախմբվածության և մասնագիտական որակների բարձրացման ճանապարհով,
                    սոցիալական, հոգեբանական և իրավաբանական աջակցության ցուցաբերման, ինչպես նաև բուժման, ախտորոշման և
                    կանխարգելման համընդանուր մատչելիության բարձրացման միջոցով:</p>
            <h4>Մեր թիմը</h4>
        </div>
        <div class="gallery-box flex-content">
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">Անուն ազգանուն</p>
                <p class="desc">Պաշտոն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">Անուն ազգանուն</p>
                <p class="desc">Պաշտոն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">Անուն ազգանուն</p>
                <p class="desc">Պաշտոն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">Անուն ազգանուն</p>
                <p class="desc">Պաշտոն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">Անուն ազգանուն</p>
                <p class="desc">Պաշտոն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">Անուն ազգանուն</p>
                <p class="desc">Պաշտոն</p>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Մեր թիմը</a></button>
        </div>
    </div>
</section>

<section id="_our-mission-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Ծառայություններ և գործունեություն</h4>
        </div>
        <div class="gallery-box flex-content">
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">ՄԻԱՎ վարակի կանխարգելու ԹՆՕ-ների շրջանում</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">(ՄԻԱՎ-ով ապրող անձանց խնամք և աջակցություն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">ՄԻԱՎ վարակի կանխարգելու ԹՆՕ-ների շրջանում</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">(ՄԻԱՎ-ով ապրող անձանց խնամք և աջակցություն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">ՄԻԱՎ վարակի կանխարգելու ԹՆՕ-ների շրջանում</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">(ՄԻԱՎ-ով ապրող անձանց խնամք և աջակցություն</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="title">(ՄԻԱՎ-ով ապրող անձանց խնամք և աջակցություն</p>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Ծառայություններ և գործունեություն</a></button>
        </div>
    </div>
    </div>
</section>

<section id="_coalitions-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Ցանցեր և կոալիցիաներ</h4>
            <p class="txt-content-wide">ՄԻԱՎ-ով ապրող մարդկանց և նրանց ընտանիքների կյանքի որակի բարելավում` ինքնօգնության և փոխօգնության
                    շարժման հզորացում, ՄԻԱՎ դրական հանրույթի համախմբվածության և մասնագիտական որակների բարձրացման ճանապարհով,
                    սոցիալական, հոգեբանական և իրավաբանական աջակցության ցուցաբերման, ինչպես նաև բուժման, ախտորոշման և կանխարգելման
                    համընդանուր մատչելիության բարձրացման միջոցով: <br><br>ՄԻԱՎ-ով ապրող մարդկանց և նրանց ընտանիքների կյանքի որակի բարելավում`
                    ինքնօգնության և փոխօգնության շարժման հզորացում, ՄԻԱՎ դրական հանրույթի համախմբվածության և մասնագիտական որակների
                    բարձրացման ճանապարհով, սոցիալական, հոգեբանական և իրավաբանական աջակցության ցուցաբերման, ինչպես նաև բուժման,
                    ախտորոշման և կանխարգելման համընդանուր մատչելիության բարձրացման միջոցով:</p>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Ցանցեր և կոալիցիաներ</a></button>
        </div>
    </div>
</section>
<section id="_reports-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Ծրագրային հաշվետվություններ</h4>
            <p class="txt-content-wide">Հարգելի այցելու, այստեղ Դուք հնարավորություն ունեք ծանոթանալ «Իրական Աշխարհ, Իրական Մարդիկ» հասարակական
                կազմակերպության կողմից պատրաստված հոդվածներին, ինչպես նաև ՄԻԱՎ/ՁԻԱՀ-ի ոլորտի ու մեր կազմակերպության
                վերաբերյալ լրատվամիջոցների հրապարակումներին</p>
        </div>
        <div class="downloaded-content-container flex-content">
            <figure class="downloaded-content">
                <div class="pdf-size-box">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/pdf-icon.png" alt="PDF Icon">
                    </div>
                    <span class="supporting-info">.PDF (126 kb.)</span>
                </div>
                <p class="title">Զեկույց Հայաստանի Հանրապետությունում մեթադոնային փոխարինող բուժման ծրագրի կատարողականի գնահատման մասին</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <div class="download-btn-box">
                    <a href="#" download>
                        <span class="download-btn-txt">Ներբեռնել</span>
                        <i class="download-btn fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </figure>
            <figure class="downloaded-content">
                <div class="pdf-size-box">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/pdf-icon.png" alt="PDF Icon">
                    </div>
                    <span class="supporting-info">.PDF (126 kb.)</span>
                </div>
                <p class="title">Զեկույց Հայաստանի Հանրապետությունում մեթադոնային փոխարինող բուժման ծրագրի կատարողականի գնահատման մասին</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <div class="download-btn-box">
                    <a href="#" download>
                        <span class="download-btn-txt">Ներբեռնել</span>
                        <i class="download-btn fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </figure>
            <figure class="downloaded-content">
                <div class="pdf-size-box">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/pdf-icon.png" alt="PDF Icon">
                    </div>
                    <span class="supporting-info">.PDF (126 kb.)</span>
                </div>
                <p class="title">Զեկույց Հայաստանի Հանրապետությունում մեթադոնային փոխարինող բուժման ծրագրի կատարողականի գնահատման մասին</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <div class="download-btn-box">
                    <a href="#" download>
                        <span class="download-btn-txt">Ներբեռնել</span>
                        <i class="download-btn fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Ծրագրային հաշվետվություններ</a></button>
        </div>
    </div>
</section>
<section id="_work-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Աշխատանք և կամավորություն</h4>
            <p class="txt-content-wide">ՄԻԱՎ-ով ապրող մարդկանց և նրանց ընտանիքների կյանքի որակի բարելավում` ինքնօգնության և փոխօգնության
                    շարժման հզորացում, ՄԻԱՎ դրական հանրույթի համախմբվածության և մասնագիտական որակների բարձրացման ճանապարհով,
                    սոցիալական, հոգեբանական և իրավաբանական աջակցության ցուցաբերման, ինչպես նաև բուժման, ախտորոշման և կանխարգելման
                    համընդանուր մատչելիության բարձրացման միջոցով: <br><br>ՄԻԱՎ-ով ապրող մարդկանց և նրանց ընտանիքների կյանքի որակի բարելավում`
                    ինքնօգնության և փոխօգնության շարժման հզորացում, ՄԻԱՎ դրական հանրույթի համախմբվածության և մասնագիտական որակների
                    բարձրացման ճանապարհով, սոցիալական, հոգեբանական և իրավաբանական աջակցության ցուցաբերման, ինչպես նաև բուժման,
                    ախտորոշման և կանխարգելման համընդանուր մատչելիության բարձրացման միջոցով:</p>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Աշխատանք և կամավորություն</a></button>
        </div>
    </div>
</section>
