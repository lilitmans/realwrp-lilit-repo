<section class="bg-opacity">
    <div class="content clear">
        <div class="content-box">
            <h1>Donation details</h1>
        </div>
    </div>
</section>
<section id="_donation-details">
    <form action="POST" method="">
        <div class="content clear">
            <div class="buttons-box">
                <button class="btn-colored"><a href="#">Online payment</a></button>
                <button class="btn-bordered"><a href="#">Offline payment</a></button>
            </div>
            <div id="_donation-details-box">
                <div class="info-block">
                    <h4>Donation Amount *</h4>
                </div>
                <div id="_donation-amount" class="flex-content">
                    <div class="bordered-field">$5</div>
                    <div class="bordered-field">$10.00</div>
                    <div class="bordered-field">$15.00</div>
                    <div class="bordered-field">$20.00</div>
                    <div class="bordered-field">$40.00</div>
                    <div class="bordered-field">$50.00</div>
                    <div class="bordered-field">$60.00</div>
                    <div class="bordered-field">$70.00</div>
                    <div class="bordered-field">$80.00</div>
                    <div class="bordered-field">$100.00</div>
                    <div class="clear bordered-field">
                        <input type="text" class="bordered-field" placeholder="or enter your own donation amount, e.g: 85">
                    </div>
                </div>
                <div class="info-block">
                    <h4>Recurrence *</h4>
                </div>
                <div id="_recurrence-box">
                    <div id="_recurrence-select-box">
                        <div id="_recurrence-select" class="select bordered-field"><span>Select</span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <p>Select the payment interval</p>
                        <div id="_select-options-box">
                            <div>1</div>
                            <div>2</div>
                            <div>3</div>
                        </div>
                    </div>
                </div>
                <div id="_message-box">
                    <div class="info-block">
                        <h4>Message</h4>
                    </div>
                    <textarea id="_message" class="bordered-field" name="" id="" cols="30" rows="10" placeholder="Your custom message text...">

                </textarea>
                </div>
                <div id="_anonymous-donation-box">
                    <div class="info-block">
                        <h4>Anonymous donation?</h4>
                    </div>
                    <label for="_anonymous-donation">
                        <input type="checkbox">
                        Check this box to hide your personal info in our donators list</label>
                </div>
                <div id="_donator-details-box">
                    <div class="info-block">
                        <h4>Donator details</h4>
                    </div>
                    <div id="_donator-details" class="flex-content">
                        <div>
                            <label for="_name"></label>
                            <input type="text" id="_name" class="bordered-field" placeholder="First Name *">
                        </div>
                        <div>
                            <label for="_last-name"></label>
                            <input type="text" id="_last-name" class="bordered-field" placeholder="Last Name *">
                        </div>
                        <div>
                            <label for="_email"></label>
                            <input type="email" id="_email" class="bordered-field" placeholder="Email">
                        </div>
                        <div>
                            <label for="_address"></label>
                            <input type="text" id="_address" class="bordered-field" placeholder="Address">
                        </div>
                        <div>
                            <label for="_postal-code"></label>
                            <input type="number" id="_postal-code" class="bordered-field" placeholder="Postal / Zip code">
                        </div>
                    </div>
                </div>
                <div class="buttons-box submit-btn-box">
                    <button class="btn-colored"><a href="#">Submit donation</a></button>
                </div>
            </div>
        </div>
    </form>
</section>