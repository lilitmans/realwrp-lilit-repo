<section id="_announcements-heading-box" class="bg-opacity">
    <div class="bg-ellipsis-box">
        <div class="img-box">
            <img src="<?=$baseurl?>/assets/img/ellipse.png" alt="Background Image">
        </div>
        <div class="img-box">
            <img src="<?=$baseurl?>/assets/img/ellipse.png" alt="Background Image">
        </div>
    </div>
    <div class="content main-heading-box clear">
        <div class="content-box heading-box">
            <h1>Հայտարարություններ</h1>
            <p class="main-description">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ, քանի
                դեռ մեր հասարակությանը բնորոշ կլինեն անտարբերությունն ու մերժողականությունը.</p>
        </div>
    </div>
</section>
<section id="_announcements-box">
    <div class="content clear">
        <div class="gallery-box detailed-info-items-box flex-content">
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="<?=$baseurl?>/announcements/announcement">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
        </div>
    </div>
</section>
