<section id="_home-page">
    <img id="_ellipse" src="assets/img/ellipse.png" alt="Background Ellipse">
    <div class="content clear">
        <div class="content-box flex-content">
            <div>
                <div id="_about-company" class="red-line"><p>Մեր ընկերության մասին</p></div>
                <h2><span>ՄԻԱՎ-ով</span> ապրող մարդկանց և նրանց ընտանիքների կյանքի որակի բարելավում՝ ինքնօգնություն և փոխօգնություն</h2>
                <p>Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ,
                    քանի դեռ մեր հասարակությանը բնորոշ կլինեն անտարբերությունն ու մերժողականությունը.</p>
                <button class="btn-colored"><a href="#">Կապվել Մեզ հետ</a></button>
                <button class="btn-bordered"><a href="#">Մեր մասին</a></button>
            </div>
            <div>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image1.png" alt="Team Photo">
                </div>
            </div>
        </div>
        <div id="_our-doners-block">
            <p>Մեր դոնորները</p>
            <div id="_our-doners" class="flex-content">
                <div>
                    <img src="<?=$baseurl?>/assets/img/content/donor1.png">
                </div>
                <div>
                    <img src="<?=$baseurl?>/assets/img/content/donor2.png">
                </div>
                <div>
                    <img src="<?=$baseurl?>/assets/img/content/donor3.png">
                </div>
                <div>
                    <img src="<?=$baseurl?>/assets/img/content/donor4.png">
                </div>
                <div>
                    <img src="<?=$baseurl?>/assets/img/content/donor5.png">
                </div>
                <div>
                    <img src="<?=$baseurl?>/assets/img/content/donor6.png">
                </div>
            </div>
        </div>
    </div>
</section>
<section id="_map-page">
    <div class="content clear">
        <div class="hot-line-block">
            <div class="content-box flex-content">
                <span>Թեժ գիծ:</span>
                <div class="tel-numbers-block">
                    <a href="tel:+374 (98) 663-383">+374 (98) 663-383</a>
                    <a href="tel:+374 (99) 653-383">+374 (99) 653-383</a>
                </div>
            </div>
        </div>
        <div class="info-block">
            <h4>Human Rights and HIV in Armenia</h4>
            <p><a href="#">Հետևեք մեզ վերջին նորությունների վերաբերյալ</a></p>
        </div>
        <div class="map-box">
        </div>
    </div>
</section>
<section id="_news-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Նորություններ</h4>
            <p><a href="#">Հետևեք մեզ վերջին նորությունների վերաբերյալ</a></p>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բլոգ</a></button>
            <button class="btn-bordered"><a href="#">Իրադարձություններ</a></button>
            <button class="btn-bordered"><a href="#">Հայտարարություններ</a></button>
        </div>
        <div id="_news-box" class="gallery-box detailed-info-items-box flex-content">
            <figure>
                <a href="">
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                    կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
            <figure>
                <a href="">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                    <p class="heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                    <p class="txt">Մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և
                        կգործի այնքան ժամանակ, մենք կոչված ենք փոխելու աշխարհը: Մեր կազմակերպությունը գոյություն կունենա և կգործի այնքան ժամանակ</p>
                </a>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բոլոր Իրադարձությունները</a></button>
        </div>
    </div>
</section>
<section id="_articles-page">
    <div class="content clear">
        <div class="info-block">
            <h4>ՀՈԴՎԱԾՆԵՐ</h4>
            <p class="txt-content">Հարգելի այցելու, այստեղ Դուք հնարավորություն ունեք ծանոթանալ «Իրական Աշխարհ, Իրական Մարդիկ»
                հասարակական կազմակերպության կողմից պատրաստված հոդվածներին, ինչպես նաև ՄԻԱՎ/ՁԻԱՀ-ի ոլորտի
                ու մեր կազմակերպության վերաբերյալ լրատվամիջոցների հրապարակումներին</p>
        </div>
        <div class="downloaded-content-container flex-content">
            <figure class="downloaded-content">
                <div class="pdf-size-box">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/pdf-icon.png" alt="PDF Icon">
                    </div>
                    <span class="supporting-info">.PDF (126 kb.)</span>
                </div>
                <p class="title">Զեկույց Հայաստանի Հանրապետությունում մեթադոնային փոխարինող բուժման ծրագրի կատարողականի գնահատման մասին</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <div class="download-btn-box">
                    <a href="#" download>
                        <span class="download-btn-txt">Ներբեռնել</span>
                        <i class="download-btn fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </figure>
            <figure class="downloaded-content">
                <div class="pdf-size-box">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/pdf-icon.png" alt="PDF Icon">
                    </div>
                    <span class="supporting-info">.PDF (126 kb.)</span>
                </div>
                <p class="title">Զեկույց Հայաստանի Հանրապետությունում մեթադոնային փոխարինող բուժման ծրագրի կատարողականի գնահատման մասին</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <div class="download-btn-box">
                    <a href="#" download>
                        <span class="download-btn-txt">Ներբեռնել</span>
                        <i class="download-btn fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </figure>
            <figure class="downloaded-content">
                <div class="pdf-size-box">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/pdf-icon.png" alt="PDF Icon">
                    </div>
                    <span class="supporting-info">.PDF (126 kb.)</span>
                </div>
                <p class="title">Զեկույց Հայաստանի Հանրապետությունում մեթադոնային փոխարինող բուժման ծրագրի կատարողականի գնահատման մասին</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <div class="download-btn-box">
                    <a href="#" download>
                        <span class="download-btn-txt">Ներբեռնել</span>
                        <i class="download-btn fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բոլոր հոդվածները</a></button>
        </div>
    </div>
</section>
<section id="_gallery-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Պատկերասրահ</h4>
            <p><a href="#">Հետևեք մեզ վերջին նորությունների վերաբերյալ</a></p>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բոլորը</a></button>
            <button class="btn-bordered"><a href="#">Նկարներ</a></button>
            <button class="btn-bordered"><a href="#">Տեսանյութ</a></button>
        </div>
        <div class="content-box flex-content">
            <div>
                <iframe height="530" src="https://www.youtube.com/embed/4poxgP96FSs"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <p>«Ընդդեմ գենդերային բռնության ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր ակտիվության 16-օրյակ» Մի՛ եղիր անտարբեր...</p>
            </div>
            <div class="images-box flex-content">
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image3.png" alt="Gallery Image">
                </div>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image4.png" alt="Gallery Image">
                </div>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image5.png" alt="Gallery Image">
                </div>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image6.png" alt="Gallery Image">
                </div>
            </div>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Պատկերասրահ բոլորը</a></button>
        </div>
    </div>
</section>
<section id="_announcement-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Հայտարարություններ</h4>
            <p><a href="#">Հետևեք մեզ վերջին նորությունների վերաբերյալ</a></p>
        </div>
        <div id="_announcements-box" class="detailed-info-items-box flex-content">
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր:
                    Այս օրը նպատակ ունի ևս մեկ անգամ հիշեցնելու ՄԻԱՎ-ի նկատմամբ թեստավորման կարևորության մասին:
                    Օրվա նշանաբանն է «Հետազոտվի´ր և վերահսկի´ր առողջությունդ»:</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր:
                    Այս օրը նպատակ ունի ևս մեկ անգամ հիշեցնելու ՄԻԱՎ-ի նկատմամբ թեստավորման կարևորության մասին:
                    Օրվա նշանաբանն է «Հետազոտվի´ր և վերահսկի´ր առողջությունդ»:</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">❗Այսօր «Իրական Աշխարհ, Իրական Մարդիկ» ՀԿ-ում տեղի ունեցավ «Դեպքի վարում»
                    թեմայով դասընթաց։
                    <br>
                    ➡ Դասընթացի ընթացքում բանախոսը ներկայացրեց դեպքի վարման սահմանումը,
                    նպատակները, սկզբունքները, ինչպես նաև մանրամասն քննարկվեց դեպքի վարման փուլերը։
                    <br>
                    ➡ Տեսական մասը ավարտելուց հետո, դեպք վարող մասնագետը ներկայացրեց դեպքի վարման վերանայված
                    գործիքակազմը։ Այնուհետև կազմակերպության մասնագետները մի քանի դեպքերի օրինակով կիրարկեցին նոր գործիքակազմը։</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">❗Այսօր «Իրական Աշխարհ, Իրական Մարդիկ» ՀԿ-ում տեղի ունեցավ «Դեպքի վարում»
                    թեմայով դասընթաց։
                    <br>
                    ➡ Դասընթացի ընթացքում բանախոսը ներկայացրեց դեպքի վարման սահմանումը,
                    նպատակները, սկզբունքները, ինչպես նաև մանրամասն քննարկվեց դեպքի վարման փուլերը։
                    <br>
                    ➡ Տեսական մասը ավարտելուց հետո, դեպք վարող մասնագետը ներկայացրեց դեպքի վարման վերանայված
                    գործիքակազմը։ Այնուհետև կազմակերպության մասնագետները մի քանի դեպքերի օրինակով կիրարկեցին նոր գործիքակազմը։</p>
            </figure>
            <figure>
                <div class="img-box">
                    <!--                        <img src="assets/img/image2.jpg" alt="News Image">-->
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր:
                    Այս օրը նպատակ ունի ևս մեկ անգամ հիշեցնելու ՄԻԱՎ-ի նկատմամբ թեստավորման կարևորության մասին:
                    Օրվա նշանաբանն է «Հետազոտվի´ր և վերահսկի´ր առողջությունդ»:</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր:
                    Այս օրը նպատակ ունի ևս մեկ անգամ հիշեցնելու ՄԻԱՎ-ի նկատմամբ թեստավորման կարևորության մասին:
                    Օրվա նշանաբանն է «Հետազոտվի´ր և վերահսկի´ր առողջությունդ»:</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր:
                    Այս օրը նպատակ ունի ևս մեկ անգամ հիշեցնելու ՄԻԱՎ-ի նկատմամբ թեստավորման կարևորության մասին:
                    Օրվա նշանաբանն է «Հետազոտվի´ր և վերահսկի´ր առողջությունդ»:</p>
            </figure>
            <figure>
                <div class="img-box">
                    <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                </div>
                <p class="news-heading">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր</p>
                <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                <p class="news-txt">Ամեն տարի հունիսի 27-ը նշվում է որպես ՄԻԱՎ-ի հետազոտության միջազգային օր:
                    Այս օրը նպատակ ունի ևս մեկ անգամ հիշեցնելու ՄԻԱՎ-ի նկատմամբ թեստավորման կարևորության մասին:
                    Օրվա նշանաբանն է «Հետազոտվի´ր և վերահսկի´ր առողջությունդ»:</p>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բոլոր հայտարարությունները</a></button>
        </div>
    </div>
</section>
<section id="_faq-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Հաճախ տրվող հարցեր</h4>
            <p><a href="#">Հետևեք մեզ վերջին նորությունների վերաբերյալ</a></p>
        </div>
        <div id="_faq-box">
            <figure>
                <div class="question-box flex-content">
                    <div class="icon-box">
                        <i class="fa fa-plus rotated-icon"></i>
                    </div>
                    <p>Բարև ձեզ: Իմ եղբայրը ՄԻԱՎ-դրական է: Մենք տանը ունենք տնային կենդանի, որը փախել էր, և
                        երեխայիս հետ եղբայրս փորձում էր բռնել նրան: Սկզբում կենդանին կծեց եղբորս, հետո երեխայիս:
                        Կա արդյոք ինչ-որ հավանականություն, որ վիրուսը կարող է փոխանցվել այդպիսի պայմաններում:
                        Խայթոցների միջև ընկած ժամ</p>
                </div>
                <div class="response-box red-line">
                    <span>Պատասխան.</span>
                    <p>Կենցաղային ճանապարհով ՄԻԱՎ-ը չի փոխանցվում, ինչպես նաև կենդանիների միջոցով:
                        Հավելում. Եթե ձեր ընտանիքում կա ՄԻԱՎ-ով ապրող մարդ, նշանակում է ձեզ անհրաժեշտ է ուսումնասիրել
                        ՄԻԱՎ-ի մասին տեղեկատվություն, որպեսզի ձեր ընտանիքի անդամների համար ապահովեք հանգիստ և առողջ մթնոլորտ</p>
                </div>
            </figure>
            <figure>
                <div class="question-box flex-content">
                    <div class="icon-box">
                        <i class="fa fa-plus rotated-icon"></i>
                    </div>
                    <p>Բարև ձեզ: Իմ եղբայրը ՄԻԱՎ-դրական է: Մենք տանը ունենք տնային կենդանի, որը փախել էր, և
                        երեխայիս հետ եղբայրս փորձում էր բռնել նրան: Սկզբում կենդանին կծեց եղբորս, հետո երեխայիս:
                        Կա արդյոք ինչ-որ հավանականություն, որ վիրուսը կարող է փոխանցվել այդպիսի պայմաններում:
                        Խայթոցների միջև ընկած ժամ</p>
                </div>
                <div class="response-box red-line">
                    <span>Պատասխան.</span>
                    <p>Կենցաղային ճանապարհով ՄԻԱՎ-ը չի փոխանցվում, ինչպես նաև կենդանիների միջոցով:
                        Հավելում. Եթե ձեր ընտանիքում կա ՄԻԱՎ-ով ապրող մարդ, նշանակում է ձեզ անհրաժեշտ է ուսումնասիրել
                        ՄԻԱՎ-ի մասին տեղեկատվություն, որպեսզի ձեր ընտանիքի անդամների համար ապահովեք հանգիստ և առողջ մթնոլորտ</p>
                </div>
            </figure>
        </div>
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Հաճախ տրվող հարցեր</a></button>
        </div>
    </div>
</section>
<section id="_reviews-page">
    <div class="content clear">
        <div class="info-block">
            <h4>Արձագանքներ</h4>
            <p><a href="#">Հետևեք մեզ վերջին նորությունների վերաբերյալ</a></p>
        </div>
        <div id="_reviews-box">
            <div class="slider-wrapper">
                <figure>
                    <div class="reviewer-img-box img-box">
                        <img src="<?=$baseurl?>/assets/img/image3.png" alt="Reviewer Picture">
                    </div>
                    <div class="content-box">
                        <div class="img-box quote-left ">
                            <img src="<?=$baseurl?>/assets/img/quote.png" alt="Quote Left">
                        </div>
                        <p>ԼԳԲՏ անձինք նամակներ են գրել իրենց ընտանիքներին։ Կազմակերպության կամավորներն ընթերցել են
                            այդ նամակները և պատրաստել հոլովակներ։ Ներկայացնում ենք նամակներն առանց փոփոխության։</p>
                        <div class="img-box quote-right">
                            <img src="<?=$baseurl?>/assets/img/quote.png" alt="Quote Right">
                        </div>
                    </div>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                </figure>
                <figure>
                    <div class="reviewer-img-box img-box">
                        <img src="<?=$baseurl?>/assets/img/image3.png" alt="Reviewer Picture">
                    </div>
                    <div class="content-box">
                        <div class="img-box quote-left ">
                            <img src="<?=$baseurl?>/assets/img/quote.png" alt="Quote Left">
                        </div>
                        <p>ԼԳԲՏ անձինք նամակներ են գրել իրենց ընտանիքներին։ Կազմակերպության կամավորներն ընթերցել են
                            այդ նամակները և պատրաստել հոլովակներ։ Ներկայացնում ենք նամակներն առանց փոփոխության։
                            Սիրելի զոքանչ, գուցե հենց այն օրվանից, որ իմացաք՝ աղջիկ եք ունենալը.</p>
                        <div class="img-box quote-right">
                            <img src="<?=$baseurl?>/assets/img/quote.png" alt="Quote Right">
                        </div>
                    </div>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                </figure>
                <figure>
                    <div class="reviewer-img-box img-box">
                        <img src="<?=$baseurl?>/assets/img/image3.png" alt="Reviewer Picture">
                    </div>
                    <div class="content-box">
                        <div class="img-box quote-left ">
                            <img src="<?=$baseurl?>/assets/img/quote.png" alt="Quote Left">
                        </div>
                        <p>ԼԳԲՏ անձինք նամակներ են գրել իրենց ընտանիքներին։ Կազմակերպության կամավորներն ընթերցել են
                            այդ նամակները և պատրաստել հոլովակներ։ Ներկայացնում ենք նամակներն առանց փոփոխության։
                            Սիրելի զոքանչ, գուցե հենց այն օրվանից, որ իմացաք՝ աղջիկ եք ունենալը. ԼԳԲՏ անձինք նամակներ
                            են գրել իրենց ընտանիքներին։ Կազմակերպության կամավորներն ընթերցել են
                            այդ նամակները և պատրաստել հոլովակներ։ Ներկայացնում ենք նամակներն առանց փոփոխության։
                            Սիրելի զոքանչ, գուցե հենց այն օրվանից, որ իմացաք՝ աղջիկ եք ունենալը.</p>
                        <div class="img-box quote-right">
                            <img src="<?=$baseurl?>/assets/img/quote.png" alt="Quote Right">
                        </div>
                    </div>
                    <span class="supporting-info"><i class="calendar-icon fa fa-calendar-alt"></i>27.06.2020</span>
                </figure>
<!--                <button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="">Previous</button>-->
<!--                <button class="slick-next slick-arrow" aria-label="Next" type="button" style="">Next</button>-->
            </div>
        </div>
    </div>
</section>