<section class="bg-opacity">
    <div class="content clear">
        <div class="content-box">
            <h1>«Իրական Աշխարհ, Իրական Մարդիկ» ՀԿ-ն շնորհակալություն է հայտնում բոլոր այն վարորդներին և կազմակերպիչներին,
                ովքեր այսքան ժամանակ իրենց անձնվեր ու սրտացավ աշխատանքով եղել են մեր և մեր շահառուների կողքին։</h1>
        </div>
    </div>
</section>
<section id="_gallery-item-box">
    <div class="content main-heading-box clear">
        <p class="txt-content-wide">«Հաճախ մարդիկ եղածը արժևորելու համար ստիպված են լինում ինչ-որ ծայրահեղ, կրիտիկական
            իրավիճակի մեջ գտնվել, մեկի համար դա կարող է հարազատի կորուստ լինել, մյուսի համար հիվանդություն, կամ ֆինանսական
            կորուստ, երևի սա այն դեպքն էր, որ բոլոր մարդկանց այդ առումով միավորեց, ամեն մեկից ինչ-որ մի բան տանելով,
            մեկից մարդկային կյանք, մյուսից ժամանակ, առողջություն, աշխատանք, ամենասովորական շփումներ, բայց ես համաճարակին
            դրականորեն եմ նայում, առհասարակ կարծում եմ, որ համաշխարհային հասարակության համար սա մի դաս է, որ մարդիկ
            հեռացել են իրարից, հոգատար չեն միմյանց և շրջապատի նկատմամբ, չեն վստահում միմյանց։</p>
    </div>
    <div class="content clear">
        <div class="gallery-box detailed-info-items-box flex-content">
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image3.png" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image3.png" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image3.png" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
            <figure>
                <a href="#">
                    <div class="img-box">
                        <img src="<?=$baseurl?>/assets/img/image2.jpg" alt="News Image">
                    </div>
                </a>
            </figure>
        </div>
    </div>
</section>