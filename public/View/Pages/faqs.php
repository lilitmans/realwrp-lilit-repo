<section id="_faqs-heading-box" class="bg-opacity">
    <div class="content clear">
        <div class="content-box">
            <h1>Հաճախ տրվող հարցեր</h1>
            <div class="buttons-box">
                <button class="btn-colored"><a href="#">Հարց ուղարկել մեզ</a></button>
            </div>
        </div>
    </div>
</section>
<section id="_faq-page">
    <div class="content clear">
        <div class="buttons-box">
            <button class="btn-colored"><a href="#">Բոլորը</a></button>
            <button class="btn-bordered"><a href="#">ՄԻԱՎ-ի փոխանցում և կանխարգելում</a></button>
            <button class="btn-bordered"><a href="#">Մարդու իրավունքներ և ՄԻԱՎ/ՁԻԱՀ</a></button>
            <button class="btn-bordered"><a href="#">Կյանք ՄԻԱՎ-ով</a></button>
        </div>
        <div id="_faq-box">
            <figure>
                <div class="question-box flex-content">
                    <div class="icon-box">
                        <i class="fa fa-plus rotated-icon"></i>
                    </div>
                    <p>Բարև ձեզ: Իմ եղբայրը ՄԻԱՎ-դրական է: Մենք տանը ունենք տնային կենդանի, որը փախել էր, և
                        երեխայիս հետ եղբայրս փորձում էր բռնել նրան: Սկզբում կենդանին կծեց եղբորս, հետո երեխայիս:
                        Կա արդյոք ինչ-որ հավանականություն, որ վիրուսը կարող է փոխանցվել այդպիսի պայմաններում:
                        Խայթոցների միջև ընկած ժամ</p>
                </div>
                <div class="response-box red-line">
                    <span>Պատասխան.</span>
                    <p>Կենցաղային ճանապարհով ՄԻԱՎ-ը չի փոխանցվում, ինչպես նաև կենդանիների միջոցով:
                        Հավելում. Եթե ձեր ընտանիքում կա ՄԻԱՎ-ով ապրող մարդ, նշանակում է ձեզ անհրաժեշտ է ուսումնասիրել
                        ՄԻԱՎ-ի մասին տեղեկատվություն, որպեսզի ձեր ընտանիքի անդամների համար ապահովեք հանգիստ և առողջ մթնոլորտ</p>
                </div>
            </figure>
            <figure>
                <div class="question-box flex-content">
                    <div class="icon-box">
                        <i class="fa fa-plus rotated-icon"></i>
                    </div>
                    <p>Բարև ձեզ: Իմ եղբայրը ՄԻԱՎ-դրական է: Մենք տանը ունենք տնային կենդանի, որը փախել էր, և
                        երեխայիս հետ եղբայրս փորձում էր բռնել նրան: Սկզբում կենդանին կծեց եղբորս, հետո երեխայիս:
                        Կա արդյոք ինչ-որ հավանականություն, որ վիրուսը կարող է փոխանցվել այդպիսի պայմաններում:
                        Խայթոցների միջև ընկած ժամ</p>
                </div>
                <div class="response-box red-line">
                    <span>Պատասխան.</span>
                    <p>Կենցաղային ճանապարհով ՄԻԱՎ-ը չի փոխանցվում, ինչպես նաև կենդանիների միջոցով:
                        Հավելում. Եթե ձեր ընտանիքում կա ՄԻԱՎ-ով ապրող մարդ, նշանակում է ձեզ անհրաժեշտ է ուսումնասիրել
                        ՄԻԱՎ-ի մասին տեղեկատվություն, որպեսզի ձեր ընտանիքի անդամների համար ապահովեք հանգիստ և առողջ մթնոլորտ</p>
                </div>
            </figure>
        </div>
    </div>
</section>