<footer>
    <div id="_img-bg"></div>
    <div class="content clear">
        <div class="content-box flex-content">
            <div class="footer-column">
                <div class="top-box">
                    <div class="title"><a href="#">Մեր մասին</a></div>
                    <p><a href="#">Հաճախ տրվող հարցեր</a></p>
                    <p><a href="#">Կարգավորող փաստաթղթեր</a></p>
                    <p><a href="#">Կապվել</a></p>
                </div>
                <div class="buttons-box">
                    <button class="btn-important"><a href="#">DONATE</a></button>
                </div>
            </div>
            <div class="footer-column">
                <div class="top-box">
                    <div class="title"><a href="#">Հոդվածներ</a></div>
                    <div class="title"><a href="#">Հայտարարություններ</a></div>
                </div>
                <div class="buttons-box">
                    <button class="btn-bordered-inverse"><a href="#">Կապվել մեզ հետ</a></button>
                </div>
            </div>
            <div class="footer-column">
                <div class="top-box">
                    <div class="title"><a href="#">Իրադարձություններ</a></div>
                </div>

                <div class="media-box">
                    <div class="title"><a href="#">Հետևեք մեզ</a></div>
                    <div id="_youtube" class="media-item">
                        <a href="#"><i class="fab fa-youtube" aria-hidden="true"></i></a>
                    </div>
                    <div id="_facebook" class="media-item">
                        <a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                    </div>
                    <div id="_instagram" class="media-item">
                        <a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                    </div>
                    <div id="_telegram" class="media-item">
                        <a href="#"><i class="fab fa-telegram-plane" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="footer-column">
                <div class="top-box">
                    <div class="title"><a href="#">Թեժ գիծ:</a></div>
                    <div class="title"><a href="tel:+374 (98) 663-383">+374 (98) 663-383</a></div>
                    <div class="title"><a href="tel:+374 (99) 653-383">+374 (99) 653-383</a></div>
                </div>
                <div class="title"><a href="#">info@realwrp.com</a></div>
            </div>
        </div>
        <div id="_all-rights-reserved-line">
            <p>
                <span>&copy;</span>
                <span id="_year">
                    </span>
                «Իրական Աշխարհ, Իրական Մարդիկ»</p>
            <p>Բոլոր իրավունքները պաշտպանված են</p>
        </div>
    </div>
</footer>

<script>let year = new Date().getFullYear(); document.getElementById('_year').innerText = `${year.toString()}`;</script>
<script src="<?=$baseurl ?>/assets/js/jquery-3.5.1.min.js"></script>
<script src="<?=$baseurl ?>/assets/js/jquery-migrate-3.3.0.min.js"></script>
<script src="<?=$baseurl ?>/assets/slick-1.8.1/slick/slick.min.js"></script>
<script src="<?=$baseurl ?>/assets/js/script.js"></script>
</body>
</html>