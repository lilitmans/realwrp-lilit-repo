<html>
	<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>REALWRP</title>
        <link rel="apple-touch-icon" sizes="57x57" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?=$baseurl ?>/assets/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?=$baseurl ?>/assets/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseurl ?>/assets/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href=<?=$baseurl ?>/"assets/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseurl ?>/assets/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?=$baseurl ?>/assets/img/favicon/manifest.json">
        <meta name="msapplication-TileImage" content="<?=$baseurl ?>/assets/img/favicon/ms-icon-144x144.png">

        <link rel="stylesheet" type="text/css" href="<?=$baseurl ?>/assets/slick-1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="<?=$baseurl ?>/assets/slick-1.8.1/slick/slick-theme.css"/>

        <link rel="stylesheet" href="<?=$baseurl ?>/assets/fonts/fontawesome-free-5.13.1/css/all.css">
        <link rel="stylesheet" href="<?=$baseurl ?>/assets/fonts/fontawesome-free-5.13.1/css/fontawesome.min.css">
        <link rel="stylesheet" href="<?=$baseurl ?>/assets/css/style.css">
	</head>
<body>
<header>
    <div class="info_header">
        <p>ՄԻԱՎ-ով ապրող անձանց խնամք աջակցություն</p>
        <span class="row_head"><img src="<?=$baseurl?>/assets/img/content/arrow.png"></span>
        <span class="close"><img src="<?=$baseurl?>/assets/img/content/close.png"></span>
    </div>
    <div class="header">
        <div class="content clear header_wrap">
            <div class="logo">
                <a href="<?=$baseurl ?>"><img src="<?=$baseurl ?>/assets/img/content/logo.png"></a>
            </div>
            <div class="header-content">
                <div class="nav">
                    <ul>
                        <li><a href="<?=$baseurl ?>/about">Մեր մասին<img src="<?=$baseurl?>/assets/img/content/plus.png"></a></li>
                        <li><a href="<?=$baseurl ?>/announcements">Նորություններ<img src="<?=$baseurl?>/assets/img/content/plus.png"></a></li>
                        <li><a href="<?=$baseurl ?>/gallery">Գրադարան<img src="<?=$baseurl?>/assets/img/content/plus.png"></a></li>
                        <li><a href="<?=$baseurl ?>/contacts">Հետադարձ կապ<img src="<?=$baseurl?>/assets/img/content/plus.png"></a></li>
                    </ul>
                </div>
                <div class="social_nav">
                    <ul>
                        <li><a href="#"><img src="<?=$baseurl?>/assets/img/content/you.png"></a></li>
                        <li><a href="#"><img src="<?=$baseurl?>/assets/img/content/fb.png"></a></li>
                        <li><a href="#"><img src="<?=$baseurl?>/assets/img/content/insta.png"></a></li>
                        <li><a href="#"><img src="<?=$baseurl?>/assets/img/content/tel.png"></a></li>
                    </ul>
                </div>
                <div class="lang_drop">
                    <span class="flag">
                        <img src="<?=$baseurl?>/assets/img/content/flag.png">
                    </span>
                        <span class="arrow_down">
                        <img src="<?=$baseurl?>/assets/img/content/arrow_down.png">
                    </span>
                </div>
            </div>
            <div class="donate_btn">DONATE</div>
            <div class="menu-icon-box">
                <img src="<?=$baseurl?>/assets/img/content/hamburger.png" alt="">
            </div>
        </div>
    </div>
      
</header>


